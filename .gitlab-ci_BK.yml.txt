#include: 'https://raw.githubusercontent.com/checkmarx-ltd/cx-flow/develop/templates/gitlab/v3/Checkmarx.gitlab-ci.yml'

variables:
    GITLAB_URL: "${CI_SERVER_URL}"
    GITLAB_API_URL: "${CI_API_V4_URL}"
    #CHECKMARX_DOCKER_IMAGE: "cx-flow"
    CX_FLOW_BUG_TRACKER: "GitLab"
    CX_FLOW_BUG_TRACKER_IMPL: ${CX_FLOW_BUG_TRACKER}
    #CX_FLOW_EXE: "java -jar /app/cx-flow.jar"
    CX_PROJECT: "$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME"
    CHECKMARX_VERSION: "9.6"
    CHECKMARX_SETTINGS_OVERRIDE: "false"
    CHECKMARX_EXCLUDE_FILES: ""
    CHECKMARX_EXCLUDE_FOLDERS: ""
    CHECKMARX_CONFIGURATION: "Default Configuration"
    CHECKMARX_SCAN_PRESET: "Checkmarx Default"
    CX_FLOW_FILTER_SEVERITY: "High"
    CX_FLOW_FILTER_CATEGORY: ""
    CX_FLOW_FILTER_CWE: ""
    CX_FLOW_FILTER_STATUS: ""
    CX_FLOW_FILTER_STATE: ""
    CX_FLOW_ENABLED_VULNERABILITY_SCANNERS: sast
    CX_FLOW_ZIP_EXCLUDE: ".jar"
    CX_TEAM: "/CxServer/"
    CX_FLOW_BREAK_BUILD: "false"
    SCA_FILTER_SEVERITY: ""
    SCA_FILTER_SCORE: ""
    SCA_THRESHOLDS_SCORE: ""
    SCA_TEAM: ""
    GITLAB_BLOCK_MERGE: "false"
    GITLAB_ERROR_MERGE: "false"
    SECURITY_DASHBOARD_ON_MR: "false"
    PARAMS: "--logging.level.com.checkmarx.flow.custom=debug --logging.level.com.checkmarx.flow.service=debug --logging.level.com.checkmarx.flow.utils=debug --logging.level.com.checkmarx.sdk.service=debug"

    #PARAMS: "--jira.url=${JIRA_URL} --jira.username=${JIRA_USER} --jira.token=${JIRA_TOKEN} --jira.project=BK --jira.issue_type=bug --jira.opened_status='To Do','In Progress','In Review' --jira.closed_status=Done,DONE --jira.open_transition='In Progress' --jira.close_transition='Done' --jira.close_transition_value='Done' --jira.priorities.High=High --jira.priorities.Medium=Medium --jira.label_prefix='google.com' --jira.priorities.Low=Low --jira.fields.0.type=result --jira.fields.0.name=application --jira.fields.0.jira_field_name=Application --jira.fields.0.jira_field_type=label --jira.fields.1.type=sca-results --jira.fields.1.name=direct-dependency --jira.fields.1.jira_field_name=Direct --jira.fields.1.jira_field_type=single-select --jira.fields.2.type=sca-results --jira.fields.2.name=risk-score --jira.fields.2.jira_field_name='Risk Score' --jira.fields.2.jira_field_type=label   --jira.fields.3.type=sca-results --jira.fields.3.name=outdated --jira.fields.3.jira_field_name=Outdated --jira.fields.3.jira_field_type=single-select --logging.level.com.checkmarx.flow.custom=debug --logging.level.com.checkmarx.flow.service=debug --logging.level.com.checkmarx.flow.utils=debug --logging.level.com.checkmarx.sdk.service=debug" 

checkmarx-scan:
  stage: test
  rules:
    - if: '$CX_FLOW_BUG_TRACKER != "GitLabDashboard" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
  script:
    /home/taynact/Java/openjdk-8u282-b08-jre/bin/java.exe -jar /home/taynact/Java/cx-flow-1.6.46-java11.jar --spring.config.location="${CI_PROJECT_DIR}/application.yml"
          --scan 
          --app="${CI_PROJECT_NAME}"
          --namespace="${CI_PROJECT_NAMESPACE}"
          --repo-name="${CI_PROJECT_NAME}"
          --repo-url="${CI_REPOSITORY_URL}"
          --cx-team="${CX_TEAM}"
          --cx-project="${CX_PROJECT}"
          --branch="${CI_COMMIT_BRANCH}"
          --spring.profiles.active="${CX_FLOW_ENABLED_VULNERABILITY_SCANNERS}"
          --f=.     
          --checkmarx.version="${CHECKMARX_VERSION}"
          ${PARAMS}  
  tags:
      - local
